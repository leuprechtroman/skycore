/*
* Vendor for TrinityCore Transmogrification
* (c) 2013 Leupi
*/
#include "ScriptPCH.h"
#include "vote.h"

enum EquipSets{
    TANK_SET = 60000,
    TANK_SET_DRUID = 60026,
    MELEE_SET = 60004,
    HEAL_SET_PALADIN = 60007,
    HEAL_SET_PRIEST = 60018,
    HEAL_SET_SHAMAN = 60014,
    HEAL_SET_DRUID = 60022,
    ROGUE_SET = 60020,
    HUNTER_SET = 60009,
    SHAMAN_MELEE_SET = 60009,
    SHAMAN_CASTER_SET = 60012,
    DRUID_MELEE_SET = 60026,
    DRUID_CASTER_SET = 60024,
    CASTER_SET = 60016,
};

enum WeaponSets{
    WARRIOR_TANK_WEAPONS = 60001,
    WARRIOR_MELEE_WEAPONS = 60005,
    PALADIN_TANK_WEAPONS = 60002,
    PALADIN_MELEE_WEAPONS = 60006,
    PALADIN_HEAL_WEAPONS = 60031,
    DEATHKNIGHT_TANK_WEAPONS = 60032,
    DEATHKNIGHT_MELEE_WEAPONS = 60032,
    HUNTER_WEAPONS = 60010,
    SHAMAN_HEAL_WEAPONS = 60015,
    SHAMAN_MELEE_WEAPONS = 60011,
    SHAMAN_CASTER_WEAPONS = 60017,
    CASTER_WEAPONS = 60017,
    PRIEST_HEAL_WEAPONS = 60019,
    DRUID_HEAL_WEAPONS = 60023,
    DRUID_CASTER_WEAPONS = 60025,
    DRUID_TANK_WEAPONS = 60027,
    DRUID_MELEE_WEAPONS = 60028,
    ROGUE_WEAPONS = 60029,
    DRUID_OWL_WEAPONS = 60025,
};

enum BasicSet{
    BASIC_TRANSFER_SET = 60030,
};

enum AdditionalItems{
    ELE_SHAMAN_TOTEM = 38361,
};


#define TRANSFER_TOKEN          37711

enum GossipConstants{
    GOSSIP_SENDER_BASIC     = 104,
    GOSSIP_SENDER_TANK      = 101,
    GOSSIP_SENDER_HEAL      = 102,
    GOSSIP_SENDER_DAMAGE    = 103,
    GOSSIP_SENDER_EXIT      = 106,
    GOSSIP_ACTION_DEFAULT   = 1000,
    GOSSIP_ACTION_MELEE     = 1001,
    GOSSIP_ACTION_RANGE     = 1002,
};

class npc_transfer_vendor : public CreatureScript
{
	public:
	npc_transfer_vendor(): CreatureScript("npc_transfer_vendor")
    {
        subject = "Deine Gegenstände";
        text = "Vielen Spaß mit deinem Charakter. Solltest du noch Fragen haben, wende dich bitte an ein Teammitglied oder frage im Forum \r\n Dein Lemniscate-WoW Team";
    }

	bool OnGossipHello(Player* player, Creature* creature)
	{
        player->PlayerTalkClass->ClearMenus();

        uint8 playerclass = player->getClass();

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gebt mir Taschen und Startgold", GOSSIP_SENDER_BASIC, GOSSIP_ACTION_DEFAULT);

        if(canBeDamageDealer(playerclass))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gebt mir eine DD Ausstattung", GOSSIP_SENDER_DAMAGE, GOSSIP_ACTION_DEFAULT);

        //For shamans and druids: give them special options:
        if(playerclass == CLASS_DRUID || playerclass == CLASS_SHAMAN)
        {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gebt mir eine DD-Caster Ausstattung", GOSSIP_SENDER_DAMAGE, GOSSIP_ACTION_RANGE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gebt mir eine DD-Melee Ausstattung", GOSSIP_SENDER_DAMAGE, GOSSIP_ACTION_MELEE);
        }

        if(canBeHealer(playerclass))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gebt mir eine Healer Ausstattung", GOSSIP_SENDER_HEAL, GOSSIP_ACTION_DEFAULT);

        if(canBeTank(playerclass))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gebt mir eine Tank Ausstattung", GOSSIP_SENDER_TANK, GOSSIP_ACTION_DEFAULT);


        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Im Moment brauche ich nichts, Danke", GOSSIP_SENDER_EXIT, GOSSIP_ACTION_DEFAULT);

        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 actions)
	{
		WorldSession* session = player->GetSession();
		player->PlayerTalkClass->ClearMenus();

        if(sender == GOSSIP_SENDER_EXIT)
        {
            player->PlayerTalkClass->SendCloseGossip();
            return true;
        }

        if(!TakeTransferToken(player))
        {
            vote_functions::NotifyPlayer(player, "Du hast keine Glücksbringer mehr zum ausgeben!");
            return true;
        }
		switch(sender)
		{
            case GOSSIP_SENDER_BASIC:
                vote_functions::SendItemviaMail(player, subject, text, BASIC_TRANSFER_SET);
                vote_functions::NotifyPlayer(player, "Deine Bestellung wird dir per Post zugestellt.");
                player->PlayerTalkClass->SendCloseGossip();
                break;

			case GOSSIP_SENDER_DAMAGE:
                switch(player->getClass())
                {
                    case CLASS_DEATH_KNIGHT:
                        vote_functions::SendItemviaMail(player, subject, text, MELEE_SET);
                        vote_functions::SendItemviaMail(player, subject, text, DEATHKNIGHT_MELEE_WEAPONS);
                        break;
                    case CLASS_PALADIN:
                        vote_functions::SendItemviaMail(player, subject, text, MELEE_SET);
                        vote_functions::SendItemviaMail(player, subject, text, PALADIN_MELEE_WEAPONS);
                        break;
                    case CLASS_WARRIOR:
                        vote_functions::SendItemviaMail(player, subject, text, MELEE_SET);
                        vote_functions::SendItemviaMail(player, subject, text, WARRIOR_MELEE_WEAPONS);
                        break;

                    case CLASS_ROGUE:
                        vote_functions::SendItemviaMail(player, subject, text, ROGUE_SET);
                        vote_functions::SendItemviaMail(player, subject, text, ROGUE_WEAPONS);
                        break;

                    case CLASS_HUNTER:
                        vote_functions::SendItemviaMail(player, subject, text, HUNTER_SET);
                        vote_functions::SendItemviaMail(player, subject, text, HUNTER_WEAPONS);
                        break;

                    case CLASS_PRIEST:
                    case CLASS_MAGE:
                    case CLASS_WARLOCK:
                        vote_functions::SendItemviaMail(player, subject, text, CASTER_SET);
                        vote_functions::SendItemviaMail(player, subject, text, CASTER_WEAPONS);
                        break;
                        
                    case CLASS_DRUID:
                        if(actions == GOSSIP_ACTION_MELEE)
                        {
                            vote_functions::SendItemviaMail(player, subject, text, DRUID_MELEE_SET);
                            vote_functions::SendItemviaMail(player, subject, text, DRUID_MELEE_WEAPONS);
                        }
                        else
                        {
                            vote_functions::SendItemviaMail(player, subject, text, DRUID_CASTER_SET);
                            vote_functions::SendItemviaMail(player, subject, text, DRUID_OWL_WEAPONS);
                        }
                        break;

                    case CLASS_SHAMAN:
                        if(actions == GOSSIP_ACTION_MELEE)
                        {
                            vote_functions::SendItemviaMail(player, subject, text, SHAMAN_MELEE_SET);
                            vote_functions::SendItemviaMail(player, subject, text, SHAMAN_MELEE_WEAPONS);
                        }
                        else
                        {
                            vote_functions::SendItemviaMail(player, subject, text, SHAMAN_CASTER_SET);
                            vote_functions::SendItemviaMail(player, subject, text, SHAMAN_CASTER_WEAPONS);
                            vote_functions::SendItemviaMail(player, subject, text, ELE_SHAMAN_TOTEM);
                        }
                        break;

                    default:
                        vote_functions::NotifyPlayer(player, "Fehler: Klasse konnte nicht ermittelt werden!");
                        return true;
                }
                vote_functions::NotifyPlayer(player, "Deine Bestellung wird dir per Post zugestellt.");
                player->PlayerTalkClass->SendCloseGossip();
                break;

			case GOSSIP_SENDER_HEAL:
                switch(player->getClass())
                {
                    case CLASS_PALADIN:
                        vote_functions::SendItemviaMail(player, subject, text, HEAL_SET_PALADIN);
                        vote_functions::SendItemviaMail(player, subject, text, PALADIN_HEAL_WEAPONS);
                        break;

                    case CLASS_PRIEST:
                        vote_functions::SendItemviaMail(player, subject, text, HEAL_SET_PRIEST);
                        vote_functions::SendItemviaMail(player, subject, text, PRIEST_HEAL_WEAPONS);
                        break;
                        
                    case CLASS_DRUID:
                        vote_functions::SendItemviaMail(player, subject, text, HEAL_SET_DRUID);
                        vote_functions::SendItemviaMail(player, subject, text, DRUID_HEAL_WEAPONS);
                        break;

                    case CLASS_SHAMAN:
                        vote_functions::SendItemviaMail(player, subject, text, HEAL_SET_SHAMAN);
                        vote_functions::SendItemviaMail(player, subject, text, SHAMAN_HEAL_WEAPONS);
                        break;

                    default:
                        vote_functions::NotifyPlayer(player, "Fehler: Klasse konnte der Heal-Rolle nicht zugeordnet werden!");
                        return true;
                }
                vote_functions::NotifyPlayer(player, "Deine Bestellung wird dir per Post zugestellt.");
                player->PlayerTalkClass->SendCloseGossip();
                break;

			case GOSSIP_SENDER_TANK:
                switch(player->getClass())
                {
                    case CLASS_DEATH_KNIGHT:
                        vote_functions::SendItemviaMail(player, subject, text, TANK_SET);
                        vote_functions::SendItemviaMail(player, subject, text, DEATHKNIGHT_TANK_WEAPONS);
                        break;
                    case CLASS_PALADIN:
                        vote_functions::SendItemviaMail(player, subject, text, TANK_SET);
                        vote_functions::SendItemviaMail(player, subject, text, PALADIN_TANK_WEAPONS);
                        break;
                    case CLASS_WARRIOR:
                        vote_functions::SendItemviaMail(player, subject, text, TANK_SET);
                        vote_functions::SendItemviaMail(player, subject, text, WARRIOR_TANK_WEAPONS);
                        break;
                    case CLASS_DRUID:
                        vote_functions::SendItemviaMail(player, subject, text, TANK_SET_DRUID);
                        vote_functions::SendItemviaMail(player, subject, text, DRUID_TANK_WEAPONS);
                        break;
                    default:
                        vote_functions::NotifyPlayer(player, "Fehler: Klasse konnte der Tank-Rolle nicht zugeordnet werden!");
                        return true;
                }
                vote_functions::NotifyPlayer(player, "Deine Bestellung wird dir per Post zugestellt.");
                player->PlayerTalkClass->SendCloseGossip();
                break;

			default:
                vote_functions::NotifyPlayer(player, "Interner Fehler trat auf: GOSSIP_SENDER could not be associated!");
                player->PlayerTalkClass->SendCloseGossip();
                break;
		}
        return true;
	}

private:

    std::string subject;
    std::string text;

    bool TakeTransferToken(Player* player)
    {
        if(player->HasItemCount(TRANSFER_TOKEN, 1, false))
        {
            player->DestroyItemCount(TRANSFER_TOKEN, 1, true);
            player->SaveforAntiRollback();
            return true; //We were sucessfull
        }
        return false;
    }

    void NotifyPlayer(Player* player, const char *str)
	{
		ChatHandler(player->GetSession()).SendSysMessage(str);
		player->GetSession()->SendAreaTriggerMessage(str);
	}

    bool canBeDamageDealer(uint8 classtotest)
    {
        switch(classtotest)
        {
            case CLASS_SHAMAN:
            case CLASS_DRUID:
            case CLASS_NONE:
                return false; //Exclude druids and shamans. we will get to them in an own if condition, they are special!
                break;
            default: return true;
        }
    }

    bool canBeHealer(uint8 classtotest)
    {
        switch(classtotest)
        {
            case CLASS_PALADIN:
            case CLASS_PRIEST:
            case CLASS_SHAMAN:
            case CLASS_DRUID:
                return true;
            default: return false;
        }
    }

    bool canBeTank(uint8 classtotest)
    {
        switch(classtotest)
        {
            case CLASS_WARRIOR:
            case CLASS_PALADIN:
            case CLASS_DEATH_KNIGHT:
            case CLASS_DRUID:
                return true;
                break;
            default: return false;
        }
    }
};

void AddSC_npc_transfer_vendor()
{
	new npc_transfer_vendor();
}

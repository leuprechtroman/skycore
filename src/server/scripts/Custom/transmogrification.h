/*
*	TrinityCore Transmogrification Script
*	(c)2013 Leupi - Skycore Development
*
* 	This script may only be used with my permission and is NOT open-source! 	
*	
*/

/*Necessary Database Table in characters database:
CREATE TABLE `transmogrification` (
	`guid` INT(10) UNSIGNED NOT NULL,
	`meshid` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`GUID`)
)
ENGINE=InnoDB;
*/

#ifndef DEF_TRANSMOGRIFICATION_H
#define DEF_TRANSMOGRIFICATION_H

#include "Config.h"

enum TransmogrificationResult
{
	TRMG_ERR_DEST_BAD_QUALITY,	//New Item has bad quality
	TRMG_ERR_SRC_BAD_QUALITY,	//old item has bad quality
	TRMG_ERR_SAME_MESH,			//Have same mesh
	TRMG_ERR_SAME_ITEM,			//2 Times the same
	TRMG_ERR_DEST_CANT_USE,		//Player can´t use the item to transmogrificate
	TRMG_ERR_SRC_CANT_USE,		//Player can´t use the original item
	TRMG_ERR_NOT_SAME_CLASS,	//Item has not the same class
	TRMG_ERR_NOT_SAME_SUBCLASS,	//Item has not the same subclass
	TRMG_ERR_WRONG_CLASS,
	TRMG_ERR_WRONG_TYPE,
	TRMG_ERR_NO_FISHING_POLE,
	TRMG_ERR_CANNOT_USE,
	TRMG_ERR_GENERAL,			//For everything else...
	TRMG_SUCCESS
};

//Helper Class, its directly defined in this header
class Transmogrification
{
public:
	Transmogrification(){};
	~Transmogrification(){};

	bool Enabled;
	bool PoorAllowed;
	bool NormalAllowed;
	bool UncommonAllowed;
	bool RareAllowed;
	bool EpicAllowed;
	bool LegendaryAllowed;
	bool ArtifactAllowed;
	bool HeirloomAllowed;

	void LoadConfiguration()
	{
		Enabled = sConfigMgr->GetBoolDefault("Transmogrification.Enabled", true);
		PoorAllowed = sConfigMgr->GetBoolDefault("Transmogrification.PoorAllowed",false);
		NormalAllowed = sConfigMgr->GetBoolDefault("Transmogrification.NormalAllowed",false);
		UncommonAllowed = sConfigMgr->GetBoolDefault("Transmogrification.UncommonAllowed",true);
		RareAllowed = sConfigMgr->GetBoolDefault("Transmogrification.RareAllowed",true);
		EpicAllowed = sConfigMgr->GetBoolDefault("Transmogrification.EpicAllowed",true);
		LegendaryAllowed = sConfigMgr->GetBoolDefault("Transmogrification.LegendaryAllowed",false);
		ArtifactAllowed = sConfigMgr->GetBoolDefault("Transmogrification.ArtifactAllowed",false);
		HeirloomAllowed = sConfigMgr->GetBoolDefault("Transmogrification.HeirloomAllowed",true);
		

	}

	bool QualityAllowed(uint32 ItemQuality)  //Rule set:
	{
		switch(ItemQuality)
		{
    	    case ITEM_QUALITY_POOR: return PoorAllowed;
		    case ITEM_QUALITY_NORMAL: return NormalAllowed;
		    case ITEM_QUALITY_UNCOMMON: return UncommonAllowed;
		    case ITEM_QUALITY_RARE: return RareAllowed;
		    case ITEM_QUALITY_EPIC: return EpicAllowed;
		    case ITEM_QUALITY_LEGENDARY: return LegendaryAllowed;
		    case ITEM_QUALITY_ARTIFACT: return ArtifactAllowed;
		    case ITEM_QUALITY_HEIRLOOM: return HeirloomAllowed;
		    default: return false;
		}
	}
};

#define sTransmogrificationConfig ACE_Singleton<Transmogrification, ACE_Null_Mutex>::instance()

#endif

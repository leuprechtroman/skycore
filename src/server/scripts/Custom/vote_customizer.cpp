//This is a script for an Ingame Customizer. He enables a race / name / factionchange ingame!
#include "ScriptPCH.h"
#include "Player.h"
#include "vote.h"

#define COST_VOTEPOINTS_CUSTOMIZE 		20
#define COST_VOTEPOINTS_RACE_CHANGE		30
#define COST_VOTEPOINTS_FACTION_CHANGE 	60
#define COST_VOTEPOINTS_CHAR_TRANSFER	80



class npc_vote_customizer : public CreatureScript
{
	public:
		npc_vote_customizer(): CreatureScript("npc_vote_customizer"){}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if(player->IsInCombat())
		{
			vote_functions::NotifyPlayer(player, "Ihr seid in einen Kampf verwickelt");
			return false; //Disallow Gossip when infight
		}

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Ich möchte meinen Charakter neu anpassen: 15 Punkte", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Ich möchte meine Rasse ändern: 30 Punkte", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Ich möchte zur anderen Fraktion überlaufen: 45 Punkte", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Ich möchte diesen Charakter auf einen anderen Account transferieren: 60 Punkte", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Ich möchte einen anstehenden Transfer rückgängig machen", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+11);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Im Moment brauche ich nichts, Danke", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
		player->PlayerTalkClass->SendGossipMenu(80000, creature->GetGUID());
		return true;
	}
	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 actions)
	{
		player->PlayerTalkClass->ClearMenus(); //First: Clear Gossip Menu Frame

		if(sender != GOSSIP_SENDER_MAIN)
			return false;
		
		switch(actions)
		{
			case GOSSIP_ACTION_INFO_DEF+1:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, ("Ja, ich möchte 15 Punkte ausgeben, um meinen Charakter neu anzupassen"), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+6);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nein, ich habs mir nochmal anders überlegt", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
				player->PlayerTalkClass->SendGossipMenu(80001, creature->GetGUID());
				break;
			case GOSSIP_ACTION_INFO_DEF+2:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, ("Ja, ich möchte 30 Punkte ausgeben, um die Rasse meines Charakters neu wählen zu können"), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+7);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nein, ich habs mir nochmal anders überlegt", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
				player->PlayerTalkClass->SendGossipMenu(80001, creature->GetGUID());
				break;
			case GOSSIP_ACTION_INFO_DEF+3:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, ("Ja, ich möchte 45 Punkte ausgeben, um zur anderen Fraktion zu wechseln"), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+8);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nein, ich habs mir nochmal anders überlegt", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
				player->PlayerTalkClass->SendGossipMenu(80001, creature->GetGUID());
				break;
			case GOSSIP_ACTION_INFO_DEF+4: //Display the Input:
				player->ADD_GOSSIP_ITEM_EXTENDED(0, "Ja, ich möchte den Charakter für 60 Punkte transferieren", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+9, "Seid ihr euch sicher?", 0, true);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nein, ich habs mir nochmal anders überlegt", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
				player->PlayerTalkClass->SendGossipMenu(80001, creature->GetGUID());
				break;
			case GOSSIP_ACTION_INFO_DEF+5:
				player->PlayerTalkClass->SendCloseGossip();
				break;
		    case GOSSIP_ACTION_INFO_DEF+6:
		    	if(vote_functions::TakeVoteItems(COST_VOTEPOINTS_CUSTOMIZE, player))
		    	{
		    		player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
		    		vote_functions::SendAcceptMSG(player);		    		
		    	}
		    	else
		    		vote_functions::SendRejectMSG(player);	    	
		    	break;
		    case GOSSIP_ACTION_INFO_DEF+7:
		    	if(vote_functions::TakeVoteItems(COST_VOTEPOINTS_RACE_CHANGE, player))
		    	{
		    		player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
		    		vote_functions::SendAcceptMSG(player);
		    	}
		    	else
		    		vote_functions::SendRejectMSG(player);
		    	break;
		    case GOSSIP_ACTION_INFO_DEF+8:
		    	if(vote_functions::TakeVoteItems(COST_VOTEPOINTS_FACTION_CHANGE, player))
		    	{
		    		player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
		    		vote_functions::SendAcceptMSG(player);
		    	}
		    	else
		    		vote_functions::SendRejectMSG(player);
		    	break;
		    case GOSSIP_ACTION_INFO_DEF+10: //Player wants to logout!
		    	player->PlayerTalkClass->SendCloseGossip();
		    	(player->GetSession())->LogoutPlayer(true); //Logout player and save him!
		    	break;
		    case GOSSIP_ACTION_INFO_DEF+11: //Reverting a transfer: Security Question
				if(player->IsFlaggedForCharTransfer())
				{
					vote_functions::NotifyPlayer(player, "Es wurde ein anstehender Transfer gefunden");
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Ich möchte den anstehenden Transfer abbrechen. Dieser Charakter soll auf diesem Account bleiben", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+12);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Ich möchte den anstehenden Transfer jetzt durchführen und mich somit ausloggen", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+10);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Ich möchte den anstehenden Transfer belassen aber trotzdem weiterspielen", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
					player->PlayerTalkClass->SendGossipMenu(80002, creature->GetGUID());
				}
				else
				{
					vote_functions::NotifyPlayer(player, "Es wurde kein anstehender Transfer gefunden. Daher kann keiner abgebrochen werden!");
					player->PlayerTalkClass->SendCloseGossip();
				}
		    	break;
		    case GOSSIP_ACTION_INFO_DEF+12:
		    	player->ResetCharTransferFlag();
		    	vote_functions::GiveVoteItems(COST_VOTEPOINTS_CHAR_TRANSFER, player);
		    	vote_functions::NotifyPlayer(player, "Der Charakter ist nun nichtmehr zum Transfer vorgemerkt. Die Kosten wurden euch erstattet.");
		    	player->PlayerTalkClass->SendCloseGossip();
		    	break;
		}
		return true;
	}
	bool OnGossipSelectCode( Player *player, Creature *_Creature, uint32 sender, uint32 action, const char* sCode )
    {
    	player->PlayerTalkClass->ClearMenus(); //First: Clear Gossip Menu Frame

        if(sender == GOSSIP_SENDER_MAIN)
        {
            if(action == GOSSIP_ACTION_INFO_DEF+9)
            {
            	//Convert char pointer to string:
            	std::string target(sCode);

            	if(vote_functions::TakeVoteItems(COST_VOTEPOINTS_CHAR_TRANSFER, player))
            	{
            		if(player->FlagForCharTransfer(target))
            		{
            			vote_functions::NotifyPlayer(player, "Dieser Charakter ist nun zum Transfer vorgemerkt. Bitte logt euch aus, um den Charakter zu transferieren.");
            			vote_functions::NotifyPlayer(player, "Er steht auf dem Ziel Account zur Verfügung, sobald ihr euch ausgeloggt habt.");
            			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Ich möchte mich jetzt ausloggen, um den Transfer abzuschließen (empfohlen)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+10);
		    			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Ich möchte noch weiterspielen", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
		    			player->PlayerTalkClass->SendGossipMenu(80003, _Creature->GetGUID());
            		}
            		else
            		{
            			vote_functions::NotifyPlayer(player, "Der angegebene Account existiert nicht oder hat keine freien Charakterplätze mehr!");
            			vote_functions::NotifyPlayer(player, "Der Transfer war nicht erfolgreich, ihr erhaltet eure Punkte zurück:");
            			player->PlayerTalkClass->SendCloseGossip();
            			vote_functions::GiveVoteItems(COST_VOTEPOINTS_CHAR_TRANSFER, player);
            		}
            	}
            	else
            		vote_functions::SendRejectMSG(player);

                return true;
            }
        }
        return false;
    }
};

void AddSC_npc_vote_customizer()
{
	new npc_vote_customizer;
}

#include "vote.h"

void vote_functions::SendRejectMSG(Player* player)
{
	NotifyPlayer(player, "Ihr habt nicht genug Punkte");
	player->PlayerTalkClass->SendCloseGossip();
}
void vote_functions::SendAcceptMSG(Player* player)
{
	NotifyPlayer(player, "Die Änderung wurde durchgeführt. Bitte loggt euch aus, um die Änderungen wirksam zu machen!");
	player->PlayerTalkClass->SendCloseGossip();
}
void vote_functions::NotifyPlayer(Player* player, const char *str)
{
	ChatHandler(player->GetSession()).SendSysMessage(str);
	player->GetSession()->SendAreaTriggerMessage(str);
}
bool vote_functions::TakeVoteItems(uint32 count, Player* player)
{
	if(player->HasItemCount(ITEM_VOTEPOINTS, count, false))
	{
		player->DestroyItemCount(ITEM_VOTEPOINTS, count, true);
		player->SaveforAntiRollback();
		return true; //We were sucessfull
	}
	return false;
}
void vote_functions::GiveVoteItems(uint32 count, Player* player)
{
	player->AddItem(ITEM_VOTEPOINTS, count);
	player->SaveforAntiRollback();
}
std::string vote_functions::GetItemName(const ItemTemplate* itemTemplate, WorldSession* session)
{
    std::string name = itemTemplate->Name1;
    int loc_idx = session->GetSessionDbLocaleIndex();
    if (loc_idx >= 0)
        if (ItemLocale const* il = sObjectMgr->GetItemLocale(itemTemplate->ItemId))
            sObjectMgr->GetLocaleString(il->Name, loc_idx, name);
    //Colorize Item name:
    std::string color="";
    switch(itemTemplate->Quality)
    {
        case ITEM_QUALITY_UNCOMMON: color="|cff26c426 " ; break;
        case ITEM_QUALITY_RARE: color="|cff2d2de1 " ; break;
        case ITEM_QUALITY_EPIC: color="|cff8c02cd " ; break;
    }
    //Add ItemLvl:
    std::ostringstream ilvl;
    ilvl << color << name; // << " (Stufe: " << itemTemplate->ItemLevel << ")";
    if(vote_functions::IsHeroic(itemTemplate))
        ilvl << " (Heroisch)";
    ilvl <<"|cffffffff"; //To make the next text white again!
    std::string string_to_return = ilvl.str();
    ilvl.str("");
	ilvl.clear();
    return string_to_return;
}

bool vote_functions::SendItemviaMail(Player* player, std::string subject, std::string text, uint32 entry)
{
	ItemTemplate const* item_proto = sObjectMgr->GetItemTemplate(entry);
	if (!item_proto)
		return false;

    MailDraft draft(subject, text);
    MailSender sender(MAIL_NORMAL, player->GetGUIDLow(), MAIL_STATIONERY_GM);

	SQLTransaction trans = CharacterDatabase.BeginTransaction();
	if (Item* item = Item::CreateItem(entry, 1, player))
    {
        item->SaveToDB(trans);                               // save for prevent lost at next mail load, if send fail then item will deleted
        draft.AddItem(item);
    }
    draft.SendMailTo(trans, MailReceiver(player, GUID_LOPART(player->GetGUID())), sender);
    CharacterDatabase.CommitTransaction(trans);

    vote_functions::NotifyPlayer(player, "Der Gegenstand wurde dir zugeschickt");
    return true;
}
bool vote_functions::IsHeroic(const ItemTemplate* itemplate)
    {
        if(itemplate->Flags & 0x8)
            return true;
        else
            return false;
    }
/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "naxxramas.h"

 enum Noth_adds
 {
    SPELL_MORTAL_STRIKE             =32736,    
    SPELL_SHADOW_SHOCK              =30138,
    SPELL_SHADOW_SHOCK_H            =54889,
    SPELL_GUARD_BLINK               =29209,
    SPELL_ARCANE_EXPLOSION          =54890,
    SPELL_ARCANE_EXPLOSION_H        =54891,
 };

enum Noth
{
    SAY_AGGRO                       = 0,
    SAY_SUMMON                      = 1,
    SAY_SLAY                        = 2,
    SAY_DEATH                       = 3,

    SOUND_DEATH                     = 8848,

    SPELL_CURSE_PLAGUEBRINGER       = 29213, // 25-man: 54835
    SPELL_CRIPPLE                   = 29212, // 25-man: 54814
    SPELL_TELEPORT                  = 29216,
    SPELL_CURSE_PLAGUEBRINGER_H     = 54835,
    SPELL_CRIPPLE_H                 = 54814,

    NPC_WARRIOR                     = 16984,
    NPC_CHAMPION                    = 16983,
    NPC_GUARDIAN                    = 16981
};

#define SPELL_BLINK                     RAND(29208, 29209, 29210, 29211)

// Teleport position of Noth on his balcony
#define TELE_X 2631.370f
#define TELE_Y -3529.680f
#define TELE_Z 274.040f
#define TELE_O 6.277f

#define MAX_SUMMON_POS 5

const float SummonPos[MAX_SUMMON_POS][4] =
{
    {2728.12f, -3544.43f, 261.91f, 6.04f},
    {2729.05f, -3544.47f, 261.91f, 5.58f},
    {2728.24f, -3465.08f, 264.20f, 3.56f},
    {2704.11f, -3456.81f, 265.53f, 4.51f},
    {2663.56f, -3464.43f, 262.66f, 5.20f},
};

enum Events
{
    EVENT_NONE,
    EVENT_BERSERK,
    EVENT_CURSE,
    EVENT_BLINK,
    EVENT_WARRIOR,
    EVENT_BALCONY,
    EVENT_WAVE,
    EVENT_GROUND,
};

class boss_noth : public CreatureScript
{
public:
    boss_noth() : CreatureScript("boss_noth") { }

    CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
        return GetInstanceAI<boss_nothAI>(creature);
    }

    struct boss_nothAI : public BossAI
    {
        boss_nothAI(Creature* creature) : BossAI(creature, BOSS_NOTH) { }

        uint32 waveCount, balconyCount;

        void Reset() OVERRIDE
        {
            me->SetReactState(REACT_AGGRESSIVE);
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
            _Reset();
        }

        void EnterCombat(Unit* /*who*/) OVERRIDE
        {
            _EnterCombat();
            Talk(SAY_AGGRO);
            balconyCount = 0;
            EnterPhaseGround();
        }

        void EnterPhaseGround()
        {
            me->SetReactState(REACT_AGGRESSIVE);
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
            DoZoneInCombat();
            if (me->getThreatManager().isThreatListEmpty())
                EnterEvadeMode();
            else
            {
                events.ScheduleEvent(EVENT_BALCONY, 110000);
                events.ScheduleEvent(EVENT_CURSE, 10000+rand()%15000);
                events.ScheduleEvent(EVENT_WARRIOR, 30000);
                if (GetDifficulty() == RAID_DIFFICULTY_25MAN_NORMAL)
                    events.ScheduleEvent(EVENT_BLINK, urand(20000, 40000));
            }
        }

        void KilledUnit(Unit* /*victim*/) OVERRIDE
        {
            if (!(rand()%5))
                Talk(SAY_SLAY);
        }

        void JustSummoned(Creature* summon) OVERRIDE
        {
            summons.Summon(summon);
            summon->setActive(true);
            summon->AI()->DoZoneInCombat();
        }

        void JustDied(Unit* /*killer*/) OVERRIDE
        {
            _JustDied();
            Talk(SAY_DEATH);
        }

        void SummonUndead(uint32 num)
        {
            //At least 2:
            if(num == 1)
                num =2;
            //Generate random count for both:
            uint32 num_champ = urand(1, num);
            uint32 num_guard = num - num_champ;

            for (uint32 i = 0; i < num_champ; ++i)
            {
                uint32 pos = rand()%MAX_SUMMON_POS;
                me->SummonCreature(NPC_CHAMPION, SummonPos[pos][0], SummonPos[pos][1], SummonPos[pos][2],
                    SummonPos[pos][3], TEMPSUMMON_CORPSE_DESPAWN, 60000);
            }
            for (uint32 i = 0; i < num_guard; ++i)
            {
                uint32 pos = rand()%MAX_SUMMON_POS;
                me->SummonCreature(NPC_GUARDIAN, SummonPos[pos][0], SummonPos[pos][1], SummonPos[pos][2],
                    SummonPos[pos][3], TEMPSUMMON_CORPSE_DESPAWN, 60000);
            }
        }
        void SummonWarriorWave(uint32 num)
        {
            for (uint32 i = 0; i < num; ++i)
            {
                uint32 pos = rand()%MAX_SUMMON_POS;
                me->SummonCreature(NPC_WARRIOR, SummonPos[pos][0], SummonPos[pos][1], SummonPos[pos][2],
                    SummonPos[pos][3], TEMPSUMMON_CORPSE_DESPAWN, 60000);
            }
        }

        void UpdateAI(uint32 diff) OVERRIDE
        {
            if (!UpdateVictim() || !CheckInRoom())
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_CURSE:
                        DoCastAOE(RAID_MODE(SPELL_CURSE_PLAGUEBRINGER, SPELL_CURSE_PLAGUEBRINGER_H));
                        events.ScheduleEvent(EVENT_CURSE, urand(50000, 60000));
                        return;
                    case EVENT_WARRIOR:
                        Talk(SAY_SUMMON);
                        SummonWarriorWave(RAID_MODE(2, 4));
                        events.ScheduleEvent(EVENT_WARRIOR, 30000);
                        return;
                    case EVENT_BLINK:
                        DoCastAOE(RAID_MODE(SPELL_CRIPPLE, SPELL_CRIPPLE_H), true);
                        DoCastAOE(SPELL_BLINK);
                        DoResetThreat();
                        events.ScheduleEvent(EVENT_BLINK, 40000);
                        return;
                    case EVENT_BALCONY:
                        me->SetReactState(REACT_PASSIVE);
                        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                        me->AttackStop();
                        me->RemoveAllAuras();
                        me->NearTeleportTo(TELE_X, TELE_Y, TELE_Z, TELE_O);
                        events.Reset();
                        events.ScheduleEvent(EVENT_WAVE, urand(2000, 5000));
                        waveCount = 0;
                        return;
                    case EVENT_WAVE:
                        Talk(SAY_SUMMON);
                        switch (balconyCount)
                        {
                            case 0: SummonUndead(RAID_MODE(3, 6)); break;
                            case 1: SummonUndead(RAID_MODE(3, 6)); break;
                            case 2: SummonUndead(RAID_MODE(3, 6)); break;
                            default:SummonUndead(RAID_MODE(6, 12));break;
                        }
                        ++waveCount;
                        events.ScheduleEvent(waveCount < 2 ? EVENT_WAVE : EVENT_GROUND, urand(30000, 45000));
                        return;
                    case EVENT_GROUND:
                    {
                        ++balconyCount;
                        float x, y, z, o;
                        me->GetHomePosition(x, y, z, o);
                        me->NearTeleportTo(x, y, z, o);
                        events.ScheduleEvent(EVENT_BALCONY, 110000);
                        EnterPhaseGround();
                        return;
                    }
                }
            }

            if (me->HasReactState(REACT_AGGRESSIVE))
                DoMeleeAttackIfReady();
        }
    };

};


class npc_noth_champion : public CreatureScript
{
public:
     npc_noth_champion () : CreatureScript("npc_noth_champion") {}

     CreatureAI* GetAI(Creature* creature) const OVERRIDE
     {
        return new npc_noth_championAI(creature);
     }

     struct npc_noth_championAI : public ScriptedAI
    {
        npc_noth_championAI(Creature* creature) : ScriptedAI(creature){}    

        uint32 m_ShadowShockTimer;
        uint32 m_MortalStrikeTimer;

        void Reset() OVERRIDE
        {
            m_ShadowShockTimer    = urand(15000, 17000);
            m_MortalStrikeTimer   = urand(8500, 10000);
            Player *nearest = me->SelectNearestPlayer(130.0f);   
            if (!nearest)
                return;          
            AttackStart(nearest);
        }

        void UpdateAI(uint32 uiDiff) OVERRIDE
        {
            if(!UpdateVictim())
                return;

            if (m_MortalStrikeTimer <= uiDiff)
            {
              DoCastVictim(SPELL_MORTAL_STRIKE);
              m_MortalStrikeTimer = urand(8500, 10000);
            }
            else 
                m_MortalStrikeTimer -= uiDiff;

            if (m_ShadowShockTimer <= uiDiff)
            {
              DoCast((RAID_MODE(SPELL_SHADOW_SHOCK, SPELL_SHADOW_SHOCK_H)));
              m_ShadowShockTimer =  urand(15000, 17000);
            }
            else
                m_ShadowShockTimer -= uiDiff;

            DoMeleeAttackIfReady();
        }
    };
};

class npc_noth_guardian : public CreatureScript
{
public:
     npc_noth_guardian () : CreatureScript("npc_noth_guardian") {}

     CreatureAI* GetAI(Creature* creature) const OVERRIDE
     {
        return new npc_noth_guardianAI(creature);
     }

     struct npc_noth_guardianAI : public ScriptedAI
    {
        npc_noth_guardianAI(Creature* creature) : ScriptedAI(creature){}    

        uint32 m_BlinkTimer;
        uint32 m_ArcaneExplosionTimer;

        void Reset() OVERRIDE
        {
            m_BlinkTimer            = urand(9500, 12000);
            m_ArcaneExplosionTimer  = urand(7000, 9000);
            Player *nearest = me->SelectNearestPlayer(130.0f);   
            if (!nearest)
                return;          
            AttackStart(nearest);
        }

        void UpdateAI(uint32 uiDiff) OVERRIDE
        {
            if(!UpdateVictim())
                return;

            if (m_BlinkTimer <= uiDiff)
            {
                DoCast(SPELL_GUARD_BLINK);
                m_BlinkTimer = urand(9500, 12000);
                AttackStart(me->GetVictim());
            }
            else 
                m_BlinkTimer -= uiDiff;

            if (m_ArcaneExplosionTimer <= uiDiff)
            {
                DoCast((RAID_MODE(SPELL_ARCANE_EXPLOSION, SPELL_ARCANE_EXPLOSION_H)));
                m_ArcaneExplosionTimer =  urand(15000, 17000);
            }
            else
                m_ArcaneExplosionTimer -= uiDiff;

            DoMeleeAttackIfReady();
        }
    };
};


void AddSC_boss_noth()
{
    new boss_noth();
    new npc_noth_champion();
    new npc_noth_guardian();
}
